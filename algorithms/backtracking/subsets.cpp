// g++ --std=c++17 -O2 -Werror permutation.cpp -o permutation && ./permutation

#include <bits/stdc++.h>
using namespace std;

vector<long> arr;
long n;

void print(vector<long> arr)
{
  for (long i=0; i<arr.size(); ++i)
    cout << arr[i] << " ";
  cout << "\n";
}

void genSubsets(long i)
{
  if (i >= n)
  {
    // print(arr);
    return;
  }

  // choose putting number
  arr.push_back(i);
  genSubsets(i+1);

  // choose not putting number
  arr.pop_back();
  genSubsets(i+1);
}


int main(int argc, char const *argv[])
{
  ios::sync_with_stdio();
  cin.tie(0);

  cin >> n;
  genSubsets(0);

  return 0;
}