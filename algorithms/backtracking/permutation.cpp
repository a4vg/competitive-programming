// g++ --std=c++17 -O2 -Werror permutation.cpp -o permutation && ./permutation

#include <bits/stdc++.h>
using namespace std;

#define MAX_SIZE 10000

vector<long> arr;
bool seen[MAX_SIZE] = {0};
long n;

void print(vector<long> arr)
{
  for (long i=0; i<arr.size(); ++i)
    cout << arr[i] << " ";
  cout << "\n";
}

void permutate(long i)
{
  if (i == n)
  {
    print(arr);
    return;
  }

  for (long e=0; e<n; ++e)
  {
    if (seen[e]) continue;

    seen[e] = true;
    arr.push_back(e);

    // cout << "\ni: "<< i;
    permutate(i+1);

    seen[e] = false;
    arr.pop_back();
  }
}


int main(int argc, char const *argv[])
{
  ios::sync_with_stdio();

  cin >> n;
  permutate(0);

  return 0;
}