// g++ --std=c++17 -O2 -Werror factory-machines.cpp -o factory-machines && ./factory-machines

/**
 * https://cses.fi/problemset/task/1620/
 *
 * A factory has n machines which can be used to make products. Your goal is
 * to make a total of t products.
 * 
 * For each machine, you know the number of seconds it needs to make a single product.
 * The machines can work simultaneously, and you can freely decide their schedule.
 * 
 * What is the shortest time needed to make t products?
 *
 * Input
 * The first input line has two integers n and t: the number of machines and products.
 * The next line has n integers k1,k2,…,kn: the time needed to make a product using
 * each machine.
 * 
 * Output
 * The first input line has two integers n and t: the number of machines and products.
 * The next line has n integers k1,k2,…,kn: the time needed to make a product using
 * each machine.
 * */

#include <bits/stdc++.h>
using namespace std;

int main(int argc, char const *argv[])
{
  ios::sync_with_stdio();

  /* code */

  return 0;
}