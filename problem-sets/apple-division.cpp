// g++ --std=c++17 -O2 -Werror apple-division.cpp -o apple-division && ./apple-division

/**
 * https://cses.fi/problemset/task/1623
 *
 * There are n apples with known weights. Your task is to divide the apples
 * into two groups so that the difference between the weights of the groups
 * is minimal.
 *
 * Input
 * The first input line has an integer n: the number of apples.
 * The next line has n integers p1,p2,…,pn: the weight of each apple.
 *
 * Output
 * Print one integer: the minimum difference between the weights of the groups.
 * */

#include <bits/stdc++.h>
using namespace std;

vector<long> arr;
long n, totalSum=0, minDif=1000000000; // max p: 1e9

void print(vector<long> arr)
{
  for (long i=0; i<arr.size(); ++i)
    cout << arr[i] << " ";
  cout << "\n";
}

void genSubsets(long i, long curSum)
{
  if (i >= arr.size())
  {
    if (abs(2*curSum-totalSum) < minDif)
      minDif = abs(2*curSum-totalSum);
    return;
  }

  // choose putting number
  genSubsets(i+1, curSum+arr[i]);

  // choose not putting number
  genSubsets(i+1, curSum);
}


int main(int argc, char const *argv[])
{
  ios::sync_with_stdio();

  cin >> n;

  arr.resize(n);
  for (long i=0; i<n; ++i)
  {
    cin >> arr[i];
    totalSum += arr[i];
  }
  genSubsets(0, 0);
  cout << minDif;
  return 0;
}