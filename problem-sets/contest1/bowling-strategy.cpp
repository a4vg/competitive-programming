// g++ --std=c++17 -O2 -Werror bowling-strategy.cpp -o bowling-strategy && ./bowling-strategy

/**
 * https://www.codechef.com/COOK122B/problems/BOWLERS
 *
 * In a cricket game, an over is a set of six valid deliveries of balls performed by one player ― the
 * bowler for this over.
 * Consider a cricket game with a series of N overs (numbered 1 through N) played by K players (numbered
 * 1 through K). Each player may be the bowler for at most L overs in total, but the same player may not
 * be the bowler for any two consecutive overs. Assign exactly one bowler to each over in such a way that
 * these rules are satisfied or determine that no such assignment exists.
 *
 * Input
 * The first line of the input contains a single integer T denoting the number of test cases.
 * The description of T test cases follows.
 * 
 * The first and only line of each test case contains three space-separated integers N, K and L. 
 *
 * Output
 * For each test case:
 * If there is no valid assignment of bowlers to overs, print a single line containing the integer −1.
 * Otherwise, print a single line containing N space-separated integers. For each valid i, the i-th of
 * these integers should be the number of the player assigned as the bowler for the i-th over.
 * */

#include <bits/stdc++.h>
using namespace std;

#define REP(n) for (long i=0; i<n; ++i)

long repeat, n, k, l;

bool isvalid()
{
  return !(n>1 && k==1) && (float)n/k<=l;
}

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  cin >> repeat;
  while (repeat--)
  {
    cin >> n >> k >> l;
    if (isvalid()) REP(n) cout << i%k+1 << " ";
    else cout << -1;
    cout << "\n";
  }

  return 0;
}