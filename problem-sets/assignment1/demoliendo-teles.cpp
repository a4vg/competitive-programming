// g++ --std=c++17 -O2 -Werror demoliendo-teles.cpp -o demoliendo-teles && ./demoliendo-teles

/**
 * Se tiene q recorrer las teles y si la tele no muestra un canal i, se destruye (sum++).
 * Para esto se lleva una cuenta aparte del i (expected_i) que sigue y si la tele muestra
 * el canal i, el i se incrementa. Si el i no ha cambiado (se mantiene en 1), significa que
 * no se ha encontrado una secuencia consecutiva creciente empezando por 1 y por lo tanto
 * no se puede satisfacer el deseo de Carlos.
 * 
 * Tiempo: O(n)   Se recorren todos los canales en un loop de 0 a n
 * Espacio: O(1)  No se requieren estructuras adicionales
 **/

#include <bits/stdc++.h>
using namespace std;

#define REP(n) for (long i=0; i<n; ++i)

long n;
vector<long> a;

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;
  a.resize(n);
  REP(n) cin >> a[i];

  long sum = 0, expected_i = 1;
  REP(n)
    if (a[i]==expected_i) ++expected_i;
    else ++sum;

  if (expected_i==1) cout << -1;
  else cout << sum;

  return 0;
}