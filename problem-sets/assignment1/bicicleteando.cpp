// g++ --std=c++17 -O2 -Werror bicicleteando.cpp -o bicicleteando && ./bicicleteando

#include <bits/stdc++.h>
using namespace std;

/**
 * Se utiliza el algoritmo de permutaciones (añadir elementos no vistos a un prefijo, de
 * manera que se obtienen todas las combinaciones posibles) para obtener todos los caminos
 * posibles. Una vez que el camino generado esté completo, se calcula la distancia recorrida,
 * se agrega a sum (suma de todas las distancias) y se incrementa el contador de caminos.
 * Cuando todas las combinaciones han sido halladas, se obtiene el promedio (sum/paths).
 * 
 * Tiempo: n*T(n-1) + 1 = O(n!)   Hay n llamadas recursivas que añadirán cada vez menos elementos
 *                                pues estos ya se habrán visto. Añadir el elemento o calcular la
 *                                distancia en cada recursión toma tiempo constante.
 * 
 * Espacio: O(n)                  Para comprobar si los elementos ya han sido vistos se utiliza un
 *                                array seen de tamaño n.
 **/

#define DISTANCE(x1, y1, x2, y2) sqrt(pow(x2-x1, 2)+pow(y2-y1, 2))
#define REP(n) for (long i=0; i<n; ++i)

vector<int> permutations;
bool seen[8] = {0};
double x[8], y[8];
int n;
double paths=0, sum=0;

void permutate(long idx)
{
  if (idx >= n)
  {
    REP(n-1)
      sum += DISTANCE(x[permutations[i]], y[permutations[i]], x[permutations[i+1]], y[permutations[i+1]]);
    paths++;
    return;
  }

  REP(n)
  {
    // saltarse los elementos que ya se han visto
    if (seen[i]) continue;
    
    seen[i] = true;
    permutations.push_back(i); // añadir elemento al prefijo
    permutate(idx+1); // añadir siguiente elemento luego
                      // de marcar el presente como visto
    seen[i] = false;
    permutations.pop_back();
  }
}

int main(int argc, char const *argv[])
{
  ios::sync_with_stdio();

  cin >> n;
  REP(n)
    cin >> x[i] >> y[i];
  permutate(0);

  cout << setprecision(13) << sum/paths;

  return 0;
}