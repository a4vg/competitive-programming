// g++ --std=c++17 -O2 -Werror papitas-fritas.cpp -o papitas-fritas && ./papitas-fritas

/**
 * Un triangulo puede ser formado si para cualquier lado, la suma de los otros dos
 * lados siempre será mayor. Por lo tanto, si la suma de los dos lados con menor
 * longitud es mayor a la longitud del lado máximo es un triángulo válido.
 * Se ordenan las longitudes de las papas y se itera sobre ellas, de manera que 
 * los primeros dos lados a, b (los de menor longitud) se mantienen fijos y para el
 * tercer lado c se recorren las longitudes hacia la derecha. Si el lado c resulta
 * ser >= a+b se rompe el loop y se continúa con la siguiente combinación de a,b, pues
 * si c>=a+b y para x>=c --> x>=a+b, lo que significa que con el resto de longitudes
 * por recorrer no se podrían formar triángulos.
 * 
 * Tiempo: O(n^3)   Tres loops anidados que se ejecutan n^3 veces en el peor de los casos
 * Espacio: O(1)    No requiere estructuras auxiliares adicionales.
 **/


#include <bits/stdc++.h>
using namespace std;

#define REP(n) for (long i=0; i<n; ++i)

long n, sum=0;
vector<long> arr;

int main(int argc, char const *argv[])
{
  ios::sync_with_stdio();
  cin.tie(0);

  cin >> n;
  arr.resize(n);
  REP(n) cin >> arr[i];

  sort(arr.begin(), arr.end());

  for (long a=0; a<n-2; ++a)
    for (long b=a+1; b<n-1; ++b)
      for (long c=b+1; c<n; ++c){
        if (arr[a]+arr[b]>arr[c]) ++sum;
        else break;
      }
       

  cout << sum;
  
  return 0;
}