// g++ --std=c++17 -O2 -Werror velocidad.cpp -o velocidad && ./velocidad

/**
 * La función de la velocidad está dada por la suma del tiempo que se tarda
 * en comprar un nuevo procesador (x) + la velocidad que se tardará en ejecutar
 * el algoritmo con dicho procesador (p/pow(2, x/1.5)). Se utiliza ternary
 * search para minimizar la función.
 * 
 * Tiempo: T(n)=T(2n/3)+1: O(logn)  En cada iteración se va descartando un
 *                                  tercio de la porción de la función anterior
 * Espacio: O(1)                    No se usa ninguna estructura adicional
 * */

#include <bits/stdc++.h>
using namespace std;

#define REP for (int i=0; i<n; ++i)
#define ERROR 0.0000001
double p, a, b;

double velocidad(double x)
{
  return x+p/pow(2, x/1.5);
}

double minimize()
{
  double l, r;
  // Ternary search
  while (b-a > ERROR)
  {
    l = (2*a+b)/3;
    r = (a+2*b)/3;
    velocidad(l) < velocidad(r)? b=r : a=l;
  }
  return velocidad((a+b)/2);
}

int main(int argc, char const *argv[])
{
  ios::sync_with_stdio();
  cin.tie(0);

  cin >> p;
  a=0;
  b=p;

  cout << setprecision(10) << minimize();


  return 0;
}