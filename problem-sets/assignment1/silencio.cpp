// g++ --std=c++17 -O2 -Werror silencio.cpp -o silencio && ./silencio

/**
 * Se utiliza el algoritmo de sliding window para iterar linealmente sobre el ___. En cada
 * iteración se comprueba si el max-min han excedido c o si el tamaño del window (i_e-i_s)
 * es más grande que m. Si este fuera el caso, se mueve la ventana (++i_s), actualizando el 
 * max/min si i_s era uno de ellos, y se continúa buscando un silencio. Se considera que el
 * silencio ha sido encontrado si el tamaño del window es igual a m y max-min<=c. Se utilizan
 * dos dequeue para mantener los max y min dentro de la ventana, de manera que cada vez q la
 * ventana se extiende por el lado derecho (i_e) se comprueba si el nuevo elemento es un max o
 * min. Si lo fuera, se coloca en el front eliminando los max/min anteriores (pop_front).
 * 
 * Tiempo: O(n-m)=O(n)  El for que mueve al window se repite n-m veces y solo realiza operaciones
 *                      constantes.
 * Espacio: O(m)        En el peor de los casos (elementos ordenados), un dequeue puede tener a
*                       todos los elementos del window.
 **/

#include <bits/stdc++.h>
using namespace std;
  
#define REP(n) for (long i=0; i<n; ++i)
  
long a[1000000];
deque<long> max_i, min_i;
long n, m, c, i_s=0, i_e=0; // i start, i end
bool silence_exist = false;

void move_start()
{
  // Avanzar el start y eliminar de los dequeues si resulta ser
  // un max/min
  if (!max_i.empty() && max_i.front()==i_s) max_i.pop_front();
  if (!min_i.empty() && min_i.front()==i_s) min_i.pop_front();
  ++i_s; // avanzar
}

int main(int argc, char const *argv[])
{
  ios::sync_with_stdio(false);
  cin.tie(0);

  // Input
  cin >> n >> m >> c;
  REP(n) cin >> a[i];
  
  for (; i_s+m<=n; ++i_e)
  {
    // Actualizar maximo y minimo con dequeues. El max/min índice siempre
    // estará en el front.
    while (!max_i.empty() && a[max_i.back()] <= a[i_e]) max_i.pop_back();
    while (!min_i.empty() && a[min_i.back()] >= a[i_e]) min_i.pop_back();
    max_i.push_back(i_e);
    min_i.push_back(i_e);

    // Chequear si se incumple alguna condición
    if (a[max_i.front()]-a[min_i.front()]>c || i_e-i_s+1 > m) move_start();

    // Si no incumple ninguna condición y la longitud es m
    else if (i_e-i_s+1 == m)
    {
      // Se ha encontrado un silencio
      silence_exist = true;
      printf("%ld\n", i_s+1);
      move_start();
    }
  }
  
  if (!silence_exist) printf("NO");
  
  return 0;
}