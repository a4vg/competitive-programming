// g++ --std=c++17 -O2 -Werror papas-fritas.cpp -o papas-fritas && ./papas-fritas

/**
 * Un triangulo puede ser formado si para cualquier lado, la suma de los otros dos
 * lados siempre será mayor. Por lo tanto, si la suma de los dos lados con menor
 * longitud es mayor a la longitud del lado máximo es un triángulo válido.
 * Se ordenan las longitudes de las papas y se toma la máxima longitud (n-1) como
 * el lado c, la mínima longitud como el lado a (0) y la segunda máxima longitud (n-2)
 * como el lado b. Luego se fija el lado c y se van recorriendo los lados de b a a,
 * si los lados a+b>c, se asume que todo el resto de lados entre
 * a y b son triángulos válidos (sum+=b-a) y se prueba con un a menor (a-1). Si la
 * suma es <=c se prueba con un b mayor (b+1). Cuando se haya recorrido todos los posibles
 * a, b (b<=a), se prueba con c-1 y así.
 * 
 * Tiempo:  O(n^2)  Dos loops anidados que se ejecutan n^2 veces en el peor de los casos
 * Espacio: O(1)    No requiere estructuras auxiliares adicionales.
 **/


#include <bits/stdc++.h>
using namespace std;

#define REP(n) for (long i=0; i<n; ++i)

long n, sum=0;
vector<long> arr;

int main(int argc, char const *argv[])
{
  ios::sync_with_stdio();
  cin.tie(0);

  cin >> n;
  arr.resize(n);
  REP(n) cin >> arr[i];

  sort(arr.begin(), arr.end());

  long a=0, b=n-2, c=n-1;
  for (; c>0; --c, b=c-1, a=0)
    while (a<b)
      if (arr[a]+arr[b]>arr[c]) sum+=(b-a), --b;
      else ++a;

  cout << sum;
  
  return 0;
}