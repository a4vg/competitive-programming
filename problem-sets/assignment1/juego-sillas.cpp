// g++ --std=c++17 -O2 -Werror juego-sillas.cpp -o juego-sillas && ./juego-sillas

/**
 * Se obtienen todas las combinaciones posibles con un el algoritmo similar al de subconjuntos.
 * Sin embargo, en lugar de elegir si colocar un elemento o no en el subconjunto, en una combinación
 * se sienta a un participante a la derecha y en otra combinación a la izquierda. Estas combinaciones
 * solo se generan si no hay un participante sentado anteriormente en la silla. Una vez se tiene una
 * combinación completa, se obtiene la distancia mínima entre dos participantes y luego se compara
 * con el máximo mínimo actual. Para simular una ronda circular se agrega una silla extra imaginaria
 * al final ocupada, representando al juez.
 * 
 * Tiempo: T(n)=2T(n-1)+1 : O(2^n)  Por cada llamada se hacen dos llamadas recursivas más y el
 *                                  el tamaño del problema se reduce en 1 (idx avanza)
 * Espacio: O(1)                    Solo se necesita un array con tamaño constante (25) para verificar
 *                                  si los asientos están ocupados.
 **/


#include <bits/stdc++.h>
using namespace std;

#define REP(init, n) for (int i=init; i<n; ++i)
#define N_CHAIRS 24

int n, max_mini=0;
int d[50];
bool chairs[N_CHAIRS+1];

void combinations(int idx)
{
  if (idx==n)
  {
    int mini=N_CHAIRS;
    int last=0;
    REP(1, N_CHAIRS+1)
      if (chairs[i]){
        if (i-last<mini) mini=i-last;
        last=i;
      }
    
    if (mini>max_mini) max_mini=mini;
    return;
  }

  if (!chairs[d[idx]])
  {
    chairs[d[idx]] = true;
    combinations(idx+1);
    chairs[d[idx]] = false;
  }
  if (!chairs[N_CHAIRS-d[idx]])
  {
    chairs[N_CHAIRS-d[idx]] = true;
    combinations(idx+1);
    chairs[N_CHAIRS-d[idx]] = false;
  }
}

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;
  REP(0, n) cin >> d[i];

  chairs[0] = true; // juez
  chairs[N_CHAIRS] = true; // juez
  combinations(0);
  cout << max_mini;

  return 0;
}