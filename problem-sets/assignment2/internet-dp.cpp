// g++ --std=c++17 -O2 -Werror internet-dp.cpp -o internet-dp && ./internet-dp

#include <bits/stdc++.h>
using namespace std;

#define REP(n) for (int i=0; i<n; ++i)

int n, m, min_cost=0;
int costs[2000];
bool routers[2000][15]; // routers con pcs
bool connected_pcs[15] = {};
int dp[2000][15]; 


int new_items(int r)
{
  int new_items = 0;
  REP(n) new_items += !connected_pcs[i] && routers[r][i];
  return new_items;
}

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> m; // pcs routers
  REP(m)
  {
    int d, pci;
    cin >> costs[i]; // precio
    cin >> d;
    for (int j=0; j<d; ++j){
      cin >> pci;
      routers[i][pci-1] = true;
    }
  }

  // Rellenar bordes (casos base)
  REP(m+1) dp[0][i] = 0;
  REP(n+1) dp[i][0] = 0;

  for (int r=1; r<m+1; ++r) // routers
  {
    for (int w=1; w<n+1; ++w) // pcs conectadas
    {
      if (costs[r-1] <= w)
      {
        int val = new_items(r-1);
        dp[r][w] = min(val + dp[r-1][w-costs[r-1]], dp[r-1][w]);
        if ((val + dp[r-1][w-costs[r-1]]) < dp[r-1][w]) REP(n) if (routers[r-1][i]) connected_pcs[i] = true;
      }
      else dp[r][w] = dp[r-1][w];
    }
  }

  bool complete = true;
  REP(n) complete = complete && connected_pcs[i];

  if (complete) cout << dp[m][n];
  else cout << -1;

  return 0;
}
