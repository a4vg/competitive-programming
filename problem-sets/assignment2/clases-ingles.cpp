// g++ --std=c++17 -O2 -Werror clases-ingles.cpp -o clases-ingles && ./clases-ingles

/**
 * Para encontrar la mayor cantidad de intercambios se busca la menor
 * cantidad de 'T's presentes en un subarray de tamaño hs (total de 'H's).
 * Esta cantidad de 'T's serán las que deben ser intercambiadas.
 * 
 * Tiempo: O(n)   Se recorre el array linealmente
 * Espacio: O(1)  No se requieren estructuras adicionales
 **/ 

#include <bits/stdc++.h>
using namespace std;

#define REP(n) for (int i=0; i<n; ++i)

int n, hs=0, s=0, e=0, mini=0, c=0, maxs=0, maxe=0;
char arr[2000];

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;
  REP(n)
  {
    cin >> arr[i];
    if (arr[i] == 'H') ++hs;
  }

  REP(hs) arr[i+n] = arr[i]; // añadir primeros hs elementos al final
                             // para simular comportamiento circular

  // Extender window hasta tener una longitud hs
  while (e-s+1 <= hs) if (arr[e++]=='T') ++c;
  --e; // el e++ anterior aumenta 1 de mas

  mini = c;

  // Sliding window
  while (e<(n+hs))
  {
    c += arr[(e+1)%n] == 'T';
    c -= arr[s++%n] == 'T';

    if (c<mini) mini = c;
    e++;
  }

  cout << mini;
  
  return 0;
}