// g++ --std=c++17 -O2 -Werror internet.cpp -o internet && ./internet
  
/**
 * Se utiliza fuerza bruta para conseguir el mínimo costo.
 * Se generan los subconjuntos posibles de routers y cada
 * vez que se agrega un nuevo router al subconjunto se
 * añade a connected_pcs. Si connected_pcs se completa (todos
 * sus elementos son true), se verifica si el costo es menor
 * al mínimo. Si un router r es parte del subconjunto que
 * logra conectar todas las pcs, se guarda en mem_routers. Antes
 * de agregar un router al subconjunto se verifica si está en 
 * mem_routers pues la mejor solución con ese router ya se encontró.
 * 
 * Tiempo: O(2m^n)=O(m^n)   Por cada subconjunto (2^n) se recorre el
 *                          array de connected_pcs (tamaño m)
 * Espacio: O(m)            Se usa connected_pcs (tamaño m)
 **/ 
  
#include <bits/stdc++.h>
using namespace std;
  
#define REP(n) for (int i=0; i<n; ++i)
#define INF 999999
  
int n, m, min_cost=INF;
int costs[2000];
bool routers[2000][15]; // routers con pcs
bool mem_routers[2000];
vector<int> cur_routers;
  
void calc(int r, int cost_sum, array<bool, 12> connected_pcs)
{
  if (r==m) return;
  if (mem_routers[r]) return;
  
  // no colocar router
  calc(r+1, cost_sum, connected_pcs);

  cur_routers.push_back(r);
  cost_sum += costs[r];
  bool complete = true;
  REP(n) // union
  {
    if (routers[r][i]) connected_pcs[i] = true;
    complete = complete && connected_pcs[i];
  }
  if (complete && cost_sum<min_cost){
    for(auto& cur_r: cur_routers) mem_routers[cur_r] = true;
    min_cost=cost_sum;
    return;
  }
  
  // colocar router
  calc(r+1, cost_sum, connected_pcs);
  cur_routers.pop_back();
}
  
int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  
  cin >> n >> m; // pcs routers
  REP(m)
  {
    int d, pci;
    cin >> costs[i]; // precio
    cin >> d;
    for (int j=0; j<d; ++j){
      cin >> pci;
      routers[i][pci-1] = true;
    }
  }
  
  calc(0, 0, array<bool, 12>());
  
  if (min_cost==INF) cout << -1;
  else cout << min_cost;
  
  return 0;
}