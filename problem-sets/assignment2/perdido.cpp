// g++ --std=c++17 -O2 -Werror perdido.cpp -o perdido && ./perdido

/**
 * Pedrito se desplaza por el eje x con las Fs hasta que encuentra una T
 * y cambia al eje y. Se sabe entonces que avanza por x o y pero no si
 * en dirección positiva o negativa. Agrupando y contando los x y y contiguos
 * se obtiene un array de positivos para x y y. El problema se resume a comprobar
 * si invirtiendo el símbolo de ciertos elementos en el array se puede
 * conseguir una suma de elementos igual a la coordenada x o y deseada.
 * Para encontrar esto, se tiene dos grupos (+) (-) que cumplen (+) + (-) = suma
 * total de elementos del array, y (+) - (-) = coord x o y deseada. Haciendo
 * sistemas de ecuaciones se concluye que la suma de elementos en (+) = (suma tota + coord x o y)/2.
 * Si es posible encontrar un subarray que cumpla estas condiciones en los
 * arrays de x y y, sí es posible llegar al destino.
 * Se utiliza dynamic programming para x y para y por separado, creando una matriz
 * arr.size()+1 x target sum+1. La matriz guardará las sumas posibles con
 * los primeros i-1 (se agrega un 0 al principio, 0<=i<=n) elementos de la matriz
 * marcando true en la casilla j (0<=j<=target sum) si el subconjunto pasado (con
 * i-1 elementos) marcó como posible llegar a la suma j o a (j-elemento i).
 * 
 * Tiempo: 2*O(arr.size()+1 x target sum+1)=O(n)   Se recorren las matrices dp (x y y)
 * Espacio: 2*O(arr.size()+1 x target sum+1)=O(n)  Tamaño de las matrices dp (x y y)
 **/

#include <bits/stdc++.h>
using namespace std;

#define REP(s, n) for (int i=s; i<n; ++i)

bool is_possible(vector<int>& arr, int xy)
{ 
  int sum_array=0;
  for (int a: arr) sum_array+=a;

  // Es imposible llegar a x o y
  if (sum_array<abs(xy) || (sum_array+xy)%2) return false;

  // target sum
  int t_sum = (sum_array+xy)/2;

  // Buscar si hay algún subarray con elementos que sumen target sum

  bool dp[arr.size()+1][t_sum+1];

  // Rellenar bordes (casos bases)
  REP(0, arr.size()+1) dp[i][0] = true;
  REP(1, t_sum+1) dp[0][i] = false;

  // Rellenar interior
  REP(1, arr.size()+1)
    for (int j=1; j<t_sum+1; ++j) {
      if (j<arr[i-1]) dp[i][j] = dp[i-1][j];
      else dp[i][j] = bool(dp[i-1][j] + dp[i-1][j-arr[i-1]]);
  }

  // Devolver esquina inferior derecha
  return dp[arr.size()][t_sum];
}

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  int x, y;
  string addr;
  vector<int> xs, ys;

  cin >> addr;
  cin >> x >> y;

  int ad_i = 0;
  // Empieza a ver a la derecha, los primeros Fs no pueden ser negativos
  while (addr[ad_i] == 'F' && ad_i<addr.length()) {--x; ++ad_i;}
  bool is_x = true;
  int xc=0, yc=0;
  while (ad_i<addr.length())
  {
    if (addr[ad_i]=='T') {
      if (is_x) xs.push_back(xc);
      else ys.push_back(yc);
      is_x = !is_x;
      xc=yc=0;
    }
    else if (is_x) ++xc;
    else ++yc;
    ++ad_i;
  }
  if (xc) xs.push_back(xc);
  if (yc) ys.push_back(yc);

  if (is_possible(xs, x) && is_possible(ys, y)) cout << "Si";
  else cout << "No";

  return 0;
}