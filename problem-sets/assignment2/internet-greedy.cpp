// g++ --std=c++17 -O2 -Werror internet-greedy.cpp -o internet-greedy && ./internet-greedy

/**
 * Se utiliza fuerza bruta para conseguir el mínimo costo.
 * Se generan los subconjuntos posibles de routers y cada
 * vez que se agrega un nuevo router al subconjunto se
 * añade a pcs 
 **/ 

#include <bits/stdc++.h>
using namespace std;

#define REP(n) for (int i=0; i<n; ++i)

int n, m, min_cost=0;
int costs[2000];
bool routers[2000][15]; // routers con pcs
vector<int> s_routers_i;
bool connected_pcs[15] = {};

float bueno_bonito_barato(int r)
{
  // Contar elementos que routers[r] tenga que connected_pcs[i] no.
  // Es decir, los que aportaría si es añadido al subconjunto final
  // de routers
  int new_items = 0;
  REP(n) new_items += !connected_pcs[i] && routers[r][i];

  return (float)costs[r]/(float)(new_items+0.000001);
} 

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> m; // pcs routers
  REP(m)
  {
    int d, pci;
    cin >> costs[i]; // precio
    cin >> d;
    for (int j=0; j<d; ++j){
      cin >> pci;
      routers[i][pci-1] = true;
    }
    s_routers_i.push_back(i);
  }

  bool complete = false;
  int r=0;
  while (!complete && r<m)
  {
    complete = true;

    vector<int>::iterator sel_it = min_element( s_routers_i.begin(), s_routers_i.end(),
    []( const auto& left, const auto& right )
    {
      return bueno_bonito_barato(left) < bueno_bonito_barato(right);
    });
    
    REP(n)
    {
      if (routers[*sel_it][i]) connected_pcs[i] = true;
      complete = complete && connected_pcs[i];
    }
    min_cost+=costs[*sel_it];
    s_routers_i.erase(sel_it);
    ++r;
  }

  if (complete) cout << min_cost;
  else cout << -1;

  return 0;
}