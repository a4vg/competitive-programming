// g++ --std=c++17 -O2 -Werror subarreglos.cpp -o subarreglos && ./subarreglos

#include <bits/stdc++.h>
using namespace std;

#define REP(n) for (long i=0; i<n; ++i)
#define INFINITE pow(10, 12)

long mini, n, r;
vector<long> arr;

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;
  arr.resize(n);
  REP(n) cin >> arr[i];

  REP(n)
  {
    mini = INFINITE;
    for (long j=0; j<n; ++j)
    {
      r = arr[j]+(i-j)*(i-j);
      if (mini > r) mini = r;
      if (r==1) break;
    }

      
    cout << mini << "\n";
  }

  return 0;
}