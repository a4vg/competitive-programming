// g++ --std=c++17 -O2 -Werror mama-conejo.cpp -o mama-conejo && ./mama-conejo


/**
 * Se ordenan los pesos de los conejos y se seleccionan 3 pesos (indices 0, 1 y n).
 * Se mantiene fijo el primer peso y se van moviendo los otros dos. Si la suma es
 * mayor a la deseada, se disminuye el 3er peso , si es menor se aumenta el 2do.
 * Si se terminan todas las iteraciones y no se ha hallado la suma, se imprime -1.
 * 
 * Tiempo: O(n^2)   Por cada iteración i1, 0<=i1<n, se van moviendo los otros dos pesos
 *                  recorriendo n-i veces el array.
 * Espacio: O(n)    Se necesita otro array de tamaño n para guardar los indices
 **/ 

#include <bits/stdc++.h>
using namespace std;

#define REP(n) for (long i=0; i<n; ++i)

bool found=false;
long n, w, sum=0, i1, i2, i3;
vector<long> arr;
vector<long> arr_i;

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> w;
  arr.resize(n);
  REP(n){
    cin >> arr[i];
    arr_i.push_back(i);
  }

  sort(arr_i.begin(), arr_i.end(), [](int i,int j){return arr[i]<arr[j];});

  for (i1=0; i1<n && !found; ++i1)
  {
    i2=i1+1;
    i3=n-1;
    while (i2<i3 && !found)
    {
      sum = arr[arr_i[i1]]+arr[arr_i[i2]]+arr[arr_i[i3]];
      
      if (sum == w) {
        found = true;
        cout << arr_i[i1]+1 << " " << arr_i[i2]+1 << " " << arr_i[i3]+1;
      }
      else if (w>sum) ++i2; // aumentar suma
      else --i3;       // disminuir suma
    }
  }

  if (!found) cout << -1;

  return 0;
}