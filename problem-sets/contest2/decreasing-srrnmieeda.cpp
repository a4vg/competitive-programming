// g++ --std=c++17 -O2 -Werror decreasing-srrnmieeda.cpp -o decreasing-srrnmieeda && ./decreasing-srrnmieeda

/**
 * https://www.codechef.com/COOK123B/problems/DECREM
 *
 * You are given two integers L and R. Find the smallest non-negative integer N such that
 * N%L>N%(L+1)>…>N%(R−1)>N%R.
 * Here, % is the modulo operator, so A%B is the remainder of A after division by B. For example, 11%3=2.
 *
 * Input
 * The first line of the input contains a single integer T denoting the number of test cases.
 * 
 * The description of T test cases follows.
 * The first and only line of each test case contains two space-separated integers L and R
 *
 * Output
 * For each test case, print a single line containing one integer ― the smallest possible N, or −1
 * if no solution exists.
 * It is guaranteed that when a solution exists, the smallest solution does not exceed 1018
 * */

#include <bits/stdc++.h>
using namespace std;

#define REP(n) for (long i=0; i<n; ++i)
long T;

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  cin >> T;
  REP(T){
    long L, R;
    cin >> L >> R;
    if (L>(R-L)) cout << R << "\n";
    else {
      cout << -1 << "\n";
    }
  }

  return 0;
}