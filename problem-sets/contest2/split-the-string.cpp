// g++ --std=c++17 -O2 -Werror split-the-string.cpp -o split-the-string && ./split-the-string

/**
 * https://www.codechef.com/COOK123B/problems/SPLITIT
 *
 * You are given a string S with length N. Determine if it is possible to find two non-empty strings A and B
 * which satisfy the following conditions:
 * - A+B=S, where + denotes string concatenation
 * - B is a substring of A
 * 
 * Note: B is a substring of A if B can be obtained from A by deleting several (possibly zero) characters
 * from the beginning and several (possibly zero) characters from the end. For example, "ab" is a substring
 * of "cabd", but "ad" is not.
 *
 * Input
 * The first line of the input contains a single integer T denoting the number of test cases.
 * 
 * The description of T test cases follows.
 * The first line of each test case contains a single integer N
 * The second line contains a single string S
.
 *
 * Output
 * For each test case, print a single line containing the string "YES" if it is possible to find
 * strings A and B satisfying the given conditions or "NO" otherwise (without quotes).
 * */

#include <bits/stdc++.h>
using namespace std;

#define REP(n) for (long i=0; i<n; ++i)

long n, T;

void calc(string S)
{
  long mid = S.size()/2;
  string A=string(S.begin(), S.end()-1);
  string B=string(1, S[S.size()-1]);

  if (A.find(B) != string::npos && B.size()!=0) cout << "YES\n";
  else cout << "NO\n";
}

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  cin >> T;

  REP(T){
    string S;
    char temp;
    cin >> n;
    REP(n){
      cin >> temp;
      S+=temp;
    }

    calc(S);
  }
  

  return 0;
}