// g++ --std=c++17 -O2 -Werror weakness-poorness.cpp -o weakness-poorness && ./weakness-poorness

/**
 * https://codeforces.com/problemset/problem/578/C
 *
 * You are given a sequence of n integers a1, a2, ..., an. Determine a real
 * number x such that the weakness of the sequence a1 - x, a2 - x, ..., an - x
 * is as small as possible.
 * 
 * The weakness of a sequence is defined as the maximum value of the poorness over
 * all segments (contiguous subsequences) of a sequence.
 * 
 * The poorness of a segment is defined as the absolute value of sum of the elements
 * of segment.
 *
 * Input
 * The first line contains one integer n (1 ≤ n ≤ 200 000), the length of a sequence.
 * The second line contains n integers a1, a2, ..., an (|ai| ≤ 10 000).
 *
 * Output
 * Output a real number denoting the minimum possible weakness of a1 - x, a2 - x, ...,
 * an - x. Your answer will be considered correct if its relative or absolute error
 * doesn't exceed 1e-6.
 * */

#include <bits/stdc++.h>
using namespace std;

int n;
vector<int> seq;

double weakness(double x)
{
  // Kadane's algorithm
  // double maxSoFar=0, maxEndingHere=0;

  double mmax,mmin,sum;
  mmax=mmin=sum=0;

  for (long i=0; i<seq.size(); ++i)
  {
    double el = seq[i]-x;

    sum += el;

    if (mmax<sum)
      mmax=sum;

    if (mmin>sum)
      mmin=sum;q

    // el>maxEndingHere+el ? maxEndingHere=el : maxEndingHere=maxEndingHere+el;

    // if (maxSoFar < maxEndingHere)
    //     maxSoFar = maxEndingHere;
  }
  return abs(mmax-mmin);
}

double minimizeWeakness(double a, double b)
{
  double l, r;
  // Ternary search
  for (int i=0; i<100; ++i)
  {
    l = (2*a+b)/3;
    r = (a+2*b)/3;

    weakness(l) < weakness(r)? b=r : a=l;
  }
  return weakness(l);
}


int main(int argc, char const *argv[])
{
  ios::sync_with_stdio();

  cin >> n;
  seq.resize(n);
  for (int i=0; i<n; ++i)
    cin >> seq[i];
  
  cout << setprecision(8) << minimizeWeakness(-10000, 10000);

  return 0;
}