// g++ --std=c++17 -O2 -Werror azoteas.cpp -o azoteas && ./azoteas

/*
 * Si entre A y B existe un edificio más alto que A, el lanzamiento no es posible. Por lo tanto,
 * la altura de A tiene que ser la máxima para lograr un lanzamiento. Se debe hallar el máximo
 * en el rango [A, B-1]. Se utiliza el algoritmo de mínimo-estático visto en clase, cambiando
 * el mínimo por el máximo. También se toma en cuenta que si no hay edificios entre A y B, el
 * lanzamiento es posible (abs(b-a)<=1) o que no siempre se cumple que A<B (se realiza un swap).
 * 
 * Tiempo: O(nlogn)   Tiempo en rellenar la matrix n x logn de máximos (preprocesamiento)
 * Espacio: O(nlogn)  Tamaño de la matriz n x logn
*/

#include <bits/stdc++.h>
using namespace std;
#define REP(n) for (long i=0; i<n; ++i)

#define MAX_N 50050

long n, m, arr[50050], correct=0;
long maxs[17][50050]; // log2(50050)+1

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  cin >> n;
  cin >> m;
  REP(n){
    cin >> arr[i];
    maxs[0][i] = arr[i];
  }

  // preprocesamiento
  for (long i=1; i<log2(n)+1; ++i)
    for (long j=0; j<n-pow(2,i)+1; ++j){
      maxs[i][j] = max(maxs[i-1][j], maxs[i-1][j+long(pow(2,i-1))]);
    }

  // consultas
  REP(m){
    long a, b, near_power, expect_max;
    cin >> a >> b;
    if (abs(b-a)<=1) {
      ++correct;
      continue;
    }

    --a;
    --b;
    expect_max = a;
    if (a>b){ // para obtener el máximo en el rango, se debe cumplir a<b
      swap(a, b);
      expect_max = b;
      ++a;
    }
    else --b;

    near_power = log2(b-a);
    correct += (max(maxs[near_power][a], maxs[near_power][b-long(pow(2, near_power))+1])==arr[expect_max]);
  }

  cout << correct;

  return 0;
}
