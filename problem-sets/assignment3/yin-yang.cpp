// g++ --std=c++17 -O2 -Werror yin-yang.cpp -o yin-yang && ./yin-yang

/**
 * Se va hasta las hojas y se comienzan a calcular las combinaciones posibles de yin y yang.
 * Ya que yang no puede repetirse dos veces seguidas, las combinaciones posibles en un nodo
 * se dan por: cantidad de yings de los hijos.
 * Mientras tanto, la fórmula para los ying será: (cantidad de yings+cantidad de yangs) de los hijos.
 * El resultado final será la suma de los posibles yings+yangs.
 * 
 * Tiempo: O(n) Se recorre un dfs
 * Espacio: O(1) No se requieren estructuras adicionales
 * */

#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
#define REP(s, n) for (ll i=s; i<n; ++i)

vector<ll> adj[300000];
ll n;

pair<ll, ll> dfs(ll x, ll p){
  // first: posibles yins
  // second: posibles yangs
  pair<ll, ll> r(1,1);
  ll yin=1, yang=1;

  // Ir hasta las hojas
  for (auto u: adj[x]){
    if (u == p) continue;
    
    r = dfs(u, x);
    // r tiene un par
    yin *= (r.first + r.second) % ((ll)1e9+7);
    yang *= r.first %((ll)1e9+7); // no pueden haber dos yangs seguidos

    // para evitar overflow
    yin %= ((ll)1e9+7);
    yang %= ((ll)1e9+7);
  }

  // Desde abajo...
  return make_pair(yin, yang);
}

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  cin >> n;
  REP(1, n) {
    long u, v;
    cin >> u >> v;
    adj[u].push_back(v);
    adj[v].push_back(u);
  }

  auto p = dfs(1,1);
  cout << (p.first+p.second)% ((long)1e9+7);

  return 0;
}
