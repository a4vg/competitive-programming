// g++ --std=c++17 -O2 -Werror rebelion.cpp -o rebelion && ./rebelion

/**
 * Mientras se recorre el árbol con un dfs se va juntando grupos de dos en dos,
 * entre padre e hijo. Si se agrupan, se marca que ya tienen grupo y la cuenta
 * de caminos liberados se incrementa.
 * 
 * Tiempo: O(n) Se recorre un dfs
 * Espacio: O(n) Se requiere un array de tamaño n para ver si tienen grupo
 * */

#include <bits/stdc++.h>
using namespace std;
#define REP(s, n) for (long i=s; i<n; ++i)

vector<long> adj[300000];
bool have_group[300000];
long n, paths=0;

void dfs(long x, long p){
  // Ir hasta las hojas
  for (auto u: adj[x]){
    if (u == p) continue;
    dfs(u, x);
  }

  // Ir juntando de dos en dos (padre-hijo)
  if  (p == 0) return; // este padre no cuenta porque corresponde a la primera llamada
  if (!have_group[x] && !have_group[p]){
    have_group[x] = have_group[p] = true;
    ++paths;
  }
}

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  cin >> n;
  REP(1, n) {
    long u, v;
    cin >> u >> v;
    adj[u].push_back(v);
    adj[v].push_back(u);
  }

  dfs(1,0);
  cout << paths;

  return 0;
}
