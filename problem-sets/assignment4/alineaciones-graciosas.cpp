// g++ --std=c++17 -O2 -Werror alineaciones-graciosas.cpp -o alineaciones-graciosas && ./alineaciones-graciosas

/**
 * Se utiliza el algoritmo de dp DP edit distance con modificaciones para
 * hallar las operaciones usadas (para obtener las alineaciones) e incluir
 * la operación de modificación con costo condicional (1 o 0 dependiendo de
 * la paridad).
 * Para hallar las operaciones se utilizó una matriz auxiliar para mantener
 * la historia de operaciones (representadas por un número constante). Luego se
 * recorre la matriz desde la esquina inferior derecha (distancia final) hasta
 * el origen para construir las alineaciones. 
 * 
 * Basado en edit distance de https://www.geeksforgeeks.org/edit-distance-dp-5/
 *
 * Tiempo: O(n*m) + O(n+m) = O(n)  Construir matriz dp + Construir alineaciones
 * Espacio: 2*O(n*m) = O(n)        Matriz dp + matriz de operaciones
 */

#include <bits/stdc++.h>
using namespace std;

#define REP(n) for (long i=0; i<n; ++i)
#define MIN(x,y,z) min(min(x, y), z)
#define INSERT 0
#define REMOVE 1
#define MODIFY 2
#define NO_OP 3

string s, t;
string alineacion_s="", alineacion_t="";
long dist, n, m;

long dp[5000 + 1][5000 + 1];
long history[5000 + 1][5000 + 1];

void calc()
{
	n = s.length();
	m = t.length();

	// Casos base
	dp[0][0] = 0;
	for (long b=0; b<=m; ++b) dp[0][b] = b,	history[0][b] = INSERT; // a vacio
	for (long a=0; a<=n; ++a) dp[a][0] = a, history[a][0] = REMOVE; // b vacio
	for (long a=1; a<=n; ++a) {
		for (long b=1; b<=m; ++b) {
      // Caracteres iguales --> no hacer nada
			if (s[a - 1] == t[b - 1]) {
				dp[a][b] = dp[a - 1][b - 1];
				history[a][b] = NO_OP;
			}
      // Caracteres diferentes --> mejor resultado hasta el momento + costo de operación
			else {
				bool eq_parity = (int)(t[b-1])%2 == (int)(s[a-1])%2;
				int best = MIN(dp[a][b - 1], // Insert
										dp[a - 1][b], // Remove
										dp[a - 1][b - 1]); // Modify

				dp[a][b] = best + 1;

        // Modify va primero porque su costo puede ser menor que el resto
				if (best == dp[a-1][b-1]){
					dp[a][b] -= eq_parity;
					history[a][b] = MODIFY;
				}
        else if (best == dp[a - 1][b])
          history[a][b] = REMOVE;
				else
					history[a][b] = INSERT;
				
			}
		}
	}

  // Construir alineaciones reconstruyendo la historia de operaciones
	int i=n, j=m;
	while (i!=0 || j!=0){
		if (history[i][j] == INSERT) {
			alineacion_s += "-";
			alineacion_t += t[--j];
		}
		else if (history[i][j] == REMOVE) {
			alineacion_t += "-";
			alineacion_s += s[--i];
		}
		else { // MODIFY or NO_OP
			alineacion_s += s[--i];
			alineacion_t += t[--j];
		}
	}

  reverse(alineacion_s.begin(), alineacion_s.end());
  reverse(alineacion_t.begin(), alineacion_t.end());
}

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

	cin >> s >> t;

	calc();

	cout << dp[n][m] << "\n"; 
	cout << alineacion_s << "\n";
	cout << alineacion_t << "\n";

	return 0;
}

// a
// a

