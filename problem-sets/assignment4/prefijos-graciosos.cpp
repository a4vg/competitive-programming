// g++ --std=c++17 -O2 -Werror prefijos-graciosos.cpp -o prefijos-graciosos && ./prefijos-graciosos

/**
 * Primero se hallan los sufijos con los caracteres en las posiciones pares
 * del string (suffix array) y luego se realizan dos búsquedas binarias continuas
 * para encontrar el rango en el suffix array donde se encuentra el prefijo. Finalmente,
 * si el rango existe, se comprueba que el prefijo es del mismo tamaño que el límite inferior
 * del rango (mínima coincidencia). Los prefijos graciosos se encuentran recorriendo de 2 en 2
 * el string original.
 * 
 * Tiempo:  O(nlog^2(n)) + O(log(n)) = O(nlog^2(n)) Tiempo de creación de suffix array y búsquedas binarias
 * Espacio: O(n) Espacio por el suffix array
 */

#include <bits/stdc++.h>
using namespace std;

typedef pair<pair<long,long>,long> piii;

const long N = 5*1e5;
string s;
long suffix[N], l[2*N];

void build(string str) {
  long n = str.size();
  for (int i = 0; i < n; ++i) {
    l[i] = str[i] - 'a';
  }
  for (int k = 0; k < n; (k==0)?k=1:k*=2) {
    vector<piii> aux;
    for (int i = 0; i < n; ++i) {
        aux.push_back({{l[i], l[i + k]}, i});
    }
    sort(aux.begin(), aux.end());
    int cont = 0;
    for (int i = 0; i < n; ++i) {
      int posi = aux[i].second;
      if (i>0 && aux[i].first != aux[i-1].first) cont++;
      l[posi] = cont+1;
    }
    if (cont == n-1) break;
  }
  for (int i = 0; i <= n; ++i) {
    suffix[l[i]-1] = i;
  }
}

int main() {
  cin >> s;
  string str_suffix = ""; // solo sufijos en posiciones pares

  for (long i=1-s.length()%2; i<s.length(); i+=2)
    str_suffix += s[i];

  build(str_suffix);
  long n=s.length();
  
  // Búsquedas binarias para encontrar el rango
  long low=0;
  long up=str_suffix.length();
  long count = 0;
  for (long k=0; k<s.size(); k+=2){ // prefijo de tamaño impar
    bool existe=false;              // y coincide con sufijo en posiciones pares
    long begin1=low;
    long end1=up;
    long begin2=low;
    long end2=up;
    while(begin1<=end1 || begin2<=end2){
      if(begin1<=end1){
        long mid1=(begin1+end1)/2;
        long valmid1=str_suffix[suffix[mid1]+k/2];
        if (valmid1 == s[k]){
          existe=true;
          end1=mid1-1;
        }
        if (valmid1<s[k]) begin1=mid1+1;
        if (valmid1>s[k]) end1=mid1-1;
      }
      if (begin2<=end2){
        long mid2=(begin2+end2)/2;
        long valmid2=str_suffix[suffix[mid2]+k/2];
        if (valmid2==s[k]){
          existe=true;
          begin2=mid2+1;
        }
        if (valmid2<s[k]) begin2=mid2+1;
        if (valmid2>s[k]) end2=mid2-1;
      }
    }
    if (!existe) break;

    // Actualizar límites
    low=begin1;
    up=end2;

    // Verificar que el sufijo es del mismo tamaño que el prefijo
    if ((str_suffix.length()-suffix[low]) == (k/2+1))
      ++count;
  }

  cout << count;

  

  return 0;
}
