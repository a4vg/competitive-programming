// g++ --std=c++17 -O2 -Werror add-candies.cpp -o add-candies && ./add-candies

#include <bits/stdc++.h>
using namespace std;
#define REP(var, n) for (long var=0; var<n; ++var)

int t, n;

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  cin >> t;
  REP(i, t){
    cin >> n;
    cout << n << "\n";
    REP(j, n) cout << j+1 << " ";
    cout << "\n";
  }
  

  return 0;
}
