// g++ --std=c++17 -O2 -Werror accidentes.cpp -o accidentes && ./accidentes

/*
El menor costo de las conexiones se halla multiplicando las dos esquinas con menor
índice. Las esquinas que generan costos son las que no pueden tener un mismo número
de calles entrantes y salientes, es decir, las que tienen un número impar de calles.
Por lo tanto, el menor costo de una de las conexiones se halla multiplicando las
primeras esquinas con calles impares.
Si no existe un conexión con esquinas impares, el costo será 0.

Tiempo: O(n)  Recorrer los nodos
Espacio: O(1)  No se requieren estructuras adicionales
*/

#include <bits/stdc++.h>
using namespace std;
#define REP(n) for (long i=0; i<n; ++i)

vector<long> adj[(unsigned long)(2*1e5)+10];
long n, m, costo = 1;
int v_impar=0;

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  long u, v;
  cin >> n >> m;

  REP(m) {
    cin >> u >> v;
    adj[u-1].push_back(v-1);
    adj[v-1].push_back(u-1);
  }

  REP(n){
    if (v_impar==2) break;
    if (adj[i].size()%2 == 1) {
      ++v_impar;
      costo *= (i+1);
    }
  }
  
  cout << costo*(v_impar>=2);

  return 0;
}
