// g++ --std=c++17 -O2 -Werror reparacion.cpp -o reparacion && ./reparacion

/*
Se utiliza el algoritmo de Bridges (DFS) para hallar los caminos críticos. El
algoritmo se modifica para filtrar los bridges que lleguen a la comandancia y
finalmente se selecciona el camino cuya suma de ciudades sea menor.

Tiempo: O(n+m)  Tiempo de encontrar Bridges (un DFS)
Espacio: O(4n) + O(m) = O(n)  Arrays auxiliares para Bridges, stack de DFS y vector de bridges
*/

#include <bits/stdc++.h>
using namespace std;
#define REP(n) for (long i=0; i<n; ++i)
#define N (unsigned long)(2*1e5)+10

vector<long> adj[N];
vector<pair<long, long>> bridges;
long n, m, c=0, super_critic=0, low[N], disc_time[N] = {0}, visited[N] = {0};

bool get_bridges(long u, long parent){
  bool reached = false;
  disc_time[u] = c++;
  low[u] = disc_time[u];
  visited[u]=1;

  for (auto& v : adj[u]) {
    // Nodo no descubierto
    if (!visited[v]) {
      // Solo añadir si el bridge conduce al destino final
      if (get_bridges(v, u) || v == n-1){
        if (low[v]==disc_time[v]) bridges.push_back( { min(v, u), max(v, u) } );
        reached = true;
      }

      low[u] = min(low[u], low[v]);
    }
    // Nodo descubierto pero no es padre
    else if (v != parent) low[u]=min(low[u], disc_time[v]);
  }

  return reached;
}

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  long u, v;
  cin >> n >> m;

  REP(m) {
    cin >> u >> v;
    adj[u-1].push_back(v-1);
    adj[v-1].push_back(u-1);
  }

  get_bridges(0, 0);

  if (bridges.size()) {
    super_critic = N;
    for (auto& b: bridges)
      if (b.first+b.second < super_critic)
        super_critic = b.first+b.second+2; // +2 porque índices empiezan en 0
  }

  cout << super_critic;

  return 0;
}
