// g++ --std=c++17 -O2 -Werror subconjuntos.cpp -o subconjuntos && ./subconjuntos

/*
Se utiliza un BFS para hallar todas las distancias de todos los
nodos en X a cada nodo en Y. Al iniciar el BFS se agregan todos
los nodos en X (de donde se empezará a buscar las distancias) en
la cola. Por último, cada vez que se extrae un nodo de la cola se
evalúa si está en Y y si su distancia es menor a la mínima. 

Tiempo: O(n+m)  Tiempo del BFS
Espacio: O(2n)=O(n)  Queue para el BFS no recursivo más las distancias
*/

#include <bits/stdc++.h>
using namespace std;
#define REP(s, n) for (long i=s; i<n; ++i)

long n, m, min_dist=2*1e5;
vector<long> adj[(unsigned long)(2*1e5)+10];
bool visited[(unsigned long)(2*1e5)+10] = {0};

bool dist_exists = false;

void distancia() {
  queue <pair<long, long>> q_d;
  // Pushear todos los nodos en X
  REP(0, n/5-1+1){
    q_d.push({i, 0});
    visited[i] = true;
  }

  while (!q_d.empty()) {
      long u = q_d.front().first;
      long dist = q_d.front().second;

      // Si está en Y
      if (((4*n/5)-1)<=u && u<=(n-1) && dist<min_dist){
        min_dist = dist;
        dist_exists = true;
      }

      q_d.pop();

      for (auto& v : adj[u]) {
         if (!visited[v]) {
           q_d.push({v, dist+1});
           visited[v] = true;
         }
      }
   }
}

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  long u, v;
  cin >> n >> m;

  REP(0, m) {
    cin >> u >> v;
    adj[u-1].push_back(v-1);
    adj[v-1].push_back(u-1);
  }

  distancia();
  
  if (dist_exists)
    cout << min_dist;
  else
    cout << "INFINITO";

  return 0;
}


