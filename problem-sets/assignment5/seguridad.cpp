// g++ --std=c++17 -O2 -Werror seguridad.cpp -o seguridad && ./seguridad
/*
Para mantener seguras todas las calles, una caseta debe estar colocada al menos
una vez en una componente de un grafo fuertemente conexo. Para hallar las
componentes se utiliza el Algoritmo de Tarjan visto en clase y, como el costo
depende del îndice del nodo, se suman los cuadrados (i^2) de todos los nodos con
índice más bajo dentro de su componente (primer nodo de la componente).

Tiempo: O(n+m)  Tiempo de Tarjan (como un DFS)
Espacio: O(4n)=O(n)  Arrays auxiliares para Tarjan y stack de DFS
*/


#include <bits/stdc++.h>
using namespace std;
#define REP(s, n) for (long i=s; i<n; ++i)
#define N (unsigned long)(2*1e5)+10
#define MOD(c) (c)%((long long)1e9+7)

vector<long> adj[N];
long n, m, c, cur_scc=1, disc_time[N],low[N],scc[N], cost=0;
bool component[N] = {0};
stack<long> s;

void tarjan(long u){
  disc_time[u] = c++;
  s.push(u);
  low[u] = disc_time[u];
  for (auto& v : adj[u]) {
    // Nodo no descubierto
    if(disc_time[v] == -1){
      tarjan(v);
      low[u]=min(low[u],low[v]);
    }
    // Nodo descubierto pero sin componente
    else if(scc[v] == -1) low[u] = min(low[u],disc_time[v]);
  }
  // Cabeza de subtree
  if(low[u]==disc_time[u]) {
    long w;
    do { 
      w=s.top();
      s.pop(); 
      scc[w]=cur_scc; // registrar elementos del componente
    } while(w!=u);
    ++cur_scc;
  }
}

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  long u, v;
  cin >> n >> m;

  REP(0, m) {
    cin >> u >> v;
    adj[u-1].push_back(v-1);
  }

  REP(0,n){
    disc_time[i]=-1;
    scc[i]=-1;
  }

  REP(0,n) if (scc[i] == -1) tarjan(i);

  // Sumar el cuadrado del 1er nodo de c/componente 
  REP(0,n) {
    if (!component[scc[i]]){
      cost = MOD(cost) + MOD(MOD((i+1))*MOD((i+1)));
      component[scc[i]] = true;
    }
  }

  cout << MOD(cost);
  
  return 0;
}
