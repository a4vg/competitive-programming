// g++ --std=c++17 -O2 -Werror numero-maximo.cpp -o numero-maximo && ./numero-maximo

/*
Con un BFS se colorea un grafo para hallar si es bipartito. Se llama al
BFS más de una vez cuando el grafo no es conexo (el siguiente vértice es
es el primer nodo que no ha sido coloreado).
Si el grafo es bipartito, x es igual a:
  (número de vértices con color 1)*(número de vértices con color 1)-m

Tiempo: O(n+m)  Tiempo del BFS
Espacio: O(n)   Queue para el BFS no recursivo
*/

#include <bits/stdc++.h>
using namespace std;
#define REP(s, n) for (long i=s; i<n; ++i)
#define MOD(c) (c)%((long long)1e9+7)
#define NO_COLOREADO -1

vector<long> adj[(unsigned long)(2*1e5)];
long n,m, col1, col2, n_visited, last_visited=0;

int color[(unsigned long)(2*1e5)+1];
bool visited[(unsigned long)(2*1e5)+1] = {0};

bool bipartito(long start) {
   color[start] = 0;
   ++col2;
   queue <long> q;
   q.push(start);
   while (!q.empty()) {
      long u = q.front();
      q.pop();

      for (auto& v : adj[u]) {
         if (color[v] == NO_COLOREADO){
            color[v] = !color[u];
            col1 = (col1 + color[v]);
            col2 = (col2 + !color[v]);
            q.push(v);
         }
         else if (color[v] == color[u]) return false;
      }
   }
   return true;
}

long get_unconnected() {
  REP(last_visited, n)
    if (color[i] == NO_COLOREADO) {
      last_visited = i;
      return i;
    }
  return -1;
}

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  long u, v;
  cin >> n >> m;

  REP(1, m+1) {
    cin >> u >> v;
    adj[u-1].push_back(v-1);
    adj[v-1].push_back(u-1);
  }

  // Nodos no coloreados
  REP(0, n) color[i] = NO_COLOREADO;

  bool is_bipartito;
  bool is_connected = true;
  long next_v = 0;
  while (next_v != -1) {
    is_bipartito = bipartito(next_v);
    if (!is_bipartito) break;

    next_v = get_unconnected();
    if (next_v != -1) is_connected = false;
  }

  if (!is_bipartito) cout << "NO ES BICOLORABLE";
  else if (!is_connected) cout << "NO ES CONEXO";
  else cout << MOD(MOD(MOD(col1)*MOD(col2))-MOD(m));

  return 0;
}

