// g++ --std=c++17 -O2 -Werror chef-ipc-certificates.cpp -o chef-ipc-certificates && ./chef-ipc-certificates

/**
 * https://www.codechef.com/COOK124B/problems/IPCCERT
 **/

#include <bits/stdc++.h>
using namespace std;
#define REP(n, var) for (long var=0; var<n; ++var)

long n, k, m;

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> m >> k;

  long ans = 0;
  REP(n, i){
    long sum = 0;
    long t;
    REP(k, j) {
      cin >> t;
      sum += t;
    }
    cin >> t;
    if (t<=10 && sum>=m) ++ans;
  }
  cout << ans;

  return 0;
}
