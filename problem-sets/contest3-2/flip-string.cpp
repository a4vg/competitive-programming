// g++ --std=c++17 -O2 -Werror flip-string.cpp -o flip-string && ./flip-string

/**
 * https://www.codechef.com/COOK124B/problems/FLIP
 **/

#include <bits/stdc++.h>
using namespace std;
#define REP(n) for (long i=0; i<n; ++i)

string a,b;

void calc() {
    string a_1="", a_2="", b_1="", b_2="";
    REP(a.length()){
        if (i%2 == 0){
            a_1+=a[i];
            b_1+=b[i];
        }
        else {
            a_2+=a[i];
            b_2+=b[i];
        }
    }

    long sum = 0;
    bool last_equal = true;
    REP(a_1.length()){
        if (a_1[i]!=b_1[i] && last_equal){
            ++sum;
            last_equal=false;
        }
        if (a_1[i]==b_1[i]) last_equal=true;
    }

    last_equal = true;
    REP(a_2.length()){
        if (a_2[i]!=b_2[i] && last_equal){
            ++sum;
            last_equal=false;
        }
        if (a_2[i]==b_2[i]) last_equal=true;
    }

    cout << sum << "\n";
}

// Driver Code
int main()
{
    long t;
    cin >> t;
    REP(t) {
        cin >> a >> b;
        calc();
    }

	return 0;
}

