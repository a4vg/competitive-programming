// g++ --std=c++17 -O2 -Werror decreasing-srrnmieeda.cpp -o decreasing-srrnmieeda && ./decreasing-srrnmieeda

/*
 * https://www.codechef.com/COOK123B/problems/DECREM/
 **/ 

#include <bits/stdc++.h>
using namespace std;

#define REP(n) for (long i=0; i<n; ++i)
long T;

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  cin >> T;
  REP(T){
    long L, R;
    cin >> L >> R;
    if (L>(R-L)) cout << R << "\n";
    else {
      cout << -1 << "\n";
    }
  }

  return 0;
}
