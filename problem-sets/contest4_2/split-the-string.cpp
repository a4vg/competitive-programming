// g++ --std=c++17 -O2 -Werror split-the-string.cpp -o split-the-string && ./split-the-string

/*
 * https://www.codechef.com/COOK123B/problems/SPLITIT/
 **/ 

#include <bits/stdc++.h>
using namespace std;

#define REP(n) for (long i=0; i<n; ++i)

long n, T;

void calc(string S)
{
  long mid = S.size()/2;
  string A=string(S.begin(), S.end()-1);
  string B=string(1, S[S.size()-1]);

  if (A.find(B) != string::npos && B.size()!=0) cout << "YES\n";
  else cout << "NO\n";
}

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  cin >> T;

  REP(T){
    string S;
    char temp;
    cin >> n;
    REP(n){
      cin >> temp;
      S+=temp;
    }

    calc(S);
  }
  

  return 0;
}
