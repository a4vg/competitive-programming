#include <bits/stdc++.h>
using namespace std;
#define REP(n) for (long i=0; i<n; ++i)

long battery, n,m,t,a,b, last;
bool drop=false;

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  cin >> n >> m >> t;
  battery = n;
  last = 0;
  REP(m){
    cin >> a >> b;

    battery -= a-last;
    if (battery<=0) drop = true;

    battery += b-a;
    last = b;

    if (battery>n) battery=n;
  }

  battery -= t-last;

  if (battery>0 && !drop) cout << "Yes";
  else cout << "No";

  return 0;
}
