#include <bits/stdc++.h>
using namespace std;
#define REP(n) for (long i=0; i<n; ++i)

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  int a,b,c,d;

  cin >> a >> b >> c >> d;

  cout << min(min(min(a,b), c),d);

  return 0;
}
