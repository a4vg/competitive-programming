#include <bits/stdc++.h>
using namespace std;
#define REP(n) for (long i=0; i<n; ++i)

// from https://stackoverflow.com/a/9331125
long long nChoosek( long long n, long long k )
{
    if (k > n) return 0;
    if (k * 2 > n) k = n-k;
    if (k == 0) return 1;

    long long result = n;
    for( long long i = 2; i <= k; ++i ) {
        result *= (n-i+1);
        result /= i;
    }
    return result;
}

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(0);
  cin.tie(0);

  long long l;
  cin >> l;

  cout << nChoosek(l-1, l-12);

  return 0;
}
