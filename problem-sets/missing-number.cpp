/**
 * https://cses.fi/problemset/task/1083/
 * 
 * You are given all numbers between 1,2,…,n except one. Your task is to find the missing number.
 * 
 * Input
 * The first input line contains an integer n.
 * The second line contains n−1 numbers. Each number is distinct and between 1 and n (inclusive).
 * 
 * Output
 * Print the missing number.
 * */

#include <bits/stdc++.h>

int main(int argc, char const *argv[])
{
  unsigned long n;
  std::cin >> n;

  std::vector<bool> numbers(n);
  unsigned long temp;
  while (--n){
    std::cin >> temp;
    numbers[temp-1] = true;
  }
  
  long i;
  for (i=0 ; numbers[i] != false; ++i);
  std::cout << i+1;

  return 0;
}
